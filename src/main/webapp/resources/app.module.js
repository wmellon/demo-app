(function() {
  'use strict';

  // Defines the 'MainApp' module
  // Contains an array of the modules used.
  angular.module('mainApp', [
    'ngRoute',
    'viewUser',
  ]);
})();