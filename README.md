# Objective
The creation of a fully fleshed out crud application that deals with a general User object. To teach the entire team how to manage source control before the CIC Marketplace. To create a level of familiarity on Angular.js, SpringMVC, and Cloudant.  

# TODO List
[TODO List](https://gist.github.com/valor13111/37a4f23d5d4c9b8a157a4f9b0db1d5aa)

# Current State
  - Spring User CRUD Controller & Model 
  - Cloudant Service Integration
  - Basic Angular.js form for user post. 

# Tools
  - Git Bash
  - Eclipse Jee Oxygen
  - Java 8 (Important)
  - Decent Text Editor (Atom || VSCode)
  - Postman (Web || Desktop Client)
 
# Installation & Setup
General Instructions. Message me on Slack with any questions. 
1. Create Liberty Server instance in Eclipse.
2. Create Cloudant NoSQL DB instance on IBM Cloud.
3. Select Create Cloundant Document.
4. Add Below To Buttom of JSON.
```json
{
  "_id": "0eea9e598f89237346510c4128417fa1",
  "firstName": "Russell",
  "lastName": "Gilmore",
  "date": 12345678
}
```
5. ID Should already be created. 
6. Create Credentials.
7. Launch Eclipse Jee Oxygen.
8. Open git bash. 
9. Get Project Source. Each user is expected to create their own origin level working branch. Have SSH Keys setup. 
```sh
$ cd "yourworkspace"
$ git clone git@github.w3ibm.bluemix.net:russell-andrew-gilmore/spuds-crud.git
$ git branch branchname
```
10. Import Exisiting Maven Project in Eclipse. 
11. Update Maven Dependencies in Project. 
12. Add Project to Liberty Server.
13. There will probably be inconsistent problems at this stage.
14. Add Credentials Created Previously to CloudantService.java.
```java
    //This is where you add credentials.
    //Account and Username are the same. 
	public CloudantService() {
		CloudantClient client = ClientBuilder.account("ACCOUNT HERE")
				.username("ACCOUNT HERE")
				.password("PASSWORD HERE").build();
		database = client.database("user", false);
	}
```
15. God willing you should be able to go to the following link and see a very bad form.
16. http://localhost:9080/spuds-crud/index.html#!/user

# Todo
- We need to set up environmental variables for the cloundant creds. I am not sure how to do this yet. 
- Add views for the other CRUD functions.
- Allocate Tasks.
- Create Working Branches.
- Discuss Roles.

# Exciting Changes
Coming Soon!




