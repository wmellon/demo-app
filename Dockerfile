FROM tomcat

COPY /target/spuds-crud-1.0.0-BUILD-SNAPSHOT.war /usr/local/tomcat/webapps/spuds-crud.war

EXPOSE 8080
